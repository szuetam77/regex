package service;

import org.junit.Test;
import java.util.List;
import static org.junit.Assert.assertEquals;
public class RegexServiceTest {

    @Test
    public void shouldReturnResultFromList(){
        //given
        RegexService regexService = new RegexService();
        String text = "Some text data for test 98020345672," +
                "and text data for test 00237684657";
        //when
        int expected = 2;
        List<String> result = regexService.get(text, "\\d{11}");
    }

    @Test
    public void shouldCountPesels() {
        //given
        RegexService regexService = new RegexService();
        String text = "Some text data for test 98020345672," +
                "and text data for test 00237684657";
        //when
        int expected = 2;
        int result = regexService.countPesels(text);
        //then
        assertEquals(2, result);
    }


    @Test
    public void shouldReturnDate() {
        //given
        RegexService regexService = new RegexService();
        String text = "Some text data for test 30-01-1998," +
                "and text data for test  30-01-2000";
        //when
        int expected = 2;
        int result = regexService.countDate(text);
        //then
        assertEquals(2, result);

    }

    @Test
    public void shouldReturnPostal() {
        //given
        RegexService regexService = new RegexService();
        String text = "Some text data for test 20-098," +
                "and text data for test 12-982";
        //when
        int expected = 2;
        int result = regexService.countCodes(text);
        //then
        assertEquals(2, result);

    }

}