package service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternValidator {


    private Pattern pattern;
    private Matcher matcher;

    private static final String PATTERN = "\\d{11}";

    public PatternValidator(){
        pattern = Pattern.compile(PATTERN);
    }


    public boolean validate(final String regex){

        matcher = pattern.matcher(regex);
        return matcher.matches();

    }

}