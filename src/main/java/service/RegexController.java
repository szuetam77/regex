package service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class RegexController {



    private String peselPattern = "\\d{11}";
    private String postalCodePattern = "\\d{2}[-]\\d{3}";
    private String datePattern = "\\d{2}[-]\\d{2}[-]\\d{4}";


    @Autowired
    private RegexService regexService;

    @PostMapping("/validatePesels")
    public ResponseEntity getPesel(@RequestBody String pattern, RegexService regexService){
        return ResponseEntity.ok(regexService.countPesels(peselPattern));
    }

    @PostMapping("/validatePostalCodes")
    public ResponseEntity getPostalCodes(@RequestBody String pattern, RegexService regexService){
        return  ResponseEntity.ok(regexService.countCodes(postalCodePattern));
    }

    @PostMapping("/validateDates")
    public ResponseEntity getDates(@RequestBody String pattern, RegexService regexService){
        return ResponseEntity.ok(regexService.countDate(datePattern));
    }


//    @PostMapping("/validate")
//    public ResponseEntity getPesels(@RequestBody RegexService regexService){
//        regexService.countPesels(peselPattern);
//        return ResponseEntity.ok(HttpStatus.OK);
//    }
//
//    @PostMapping("/validate")
//    public ResponseEntity getPostalCodes(@RequestBody RegexService regexService){
//        regexService.countPesels(postalCodePattern);
//        return ResponseEntity.ok(HttpStatus.OK);
//    }
//    @PostMapping("/validate")
//    public ResponseEntity getDates(@RequestBody RegexService regexService){
//        regexService.countPesels(datePattern);
//        return ResponseEntity.ok(HttpStatus.OK);
//    }


}
