package service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexService {

    public List<String> getPesels(String source){
        Matcher matcher = Pattern.compile("\\d{11}").matcher(source);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(source.substring(matcher.start(), matcher.end()));
        }
        return result;
    }

    public List<String> getPostalCodes(String source){
        Matcher matcher = Pattern.compile("\\d{2}[-]\\d{3}").matcher(source);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(source.substring(matcher.start(), matcher.end()));
        }
        return result;
    }

    public List<String> getDates(String source){
        Matcher matcher = Pattern.compile("\\d{2}[-]\\d{2}[-]\\d{4}").matcher(source);
        List<String> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(source.substring(matcher.start(), matcher.end()));
        }
        return result;
    }

    public List<String> get(String source, String pattern){
        Matcher matcher = Pattern.compile(pattern).matcher(source);
        List<String> result = new ArrayList<>();
        while(matcher.find()){
            result.add(source.substring(matcher.start(), matcher.end()));
            System.out.println(source.substring(matcher.start(), matcher.end()));
        }
        return result;
    }

    private int count(String source, String pattern){
        Matcher matcher = Pattern.compile(pattern).matcher(source);
        int count = 0;
        while (matcher.find()) {
            System.out.println(source.substring(matcher.start(), matcher.end()));
            count++;
        }
        return count;
    }

    public int countPesels(String source) {
        return count(source, "\\d{11}");
    }

    //countDate (\\d{2}[-]\\d{2}[-]\\d{4})

    public int countDate(String source){

        return count(source, "\\d{2}[-]\\d{2}[-]\\d{4}");
    }

    public int countCodes (String source){

        return count(source, "\\d{2}[-]\\d{3}");
    }



}
